# shellcheck shell=bash

shellspec_syntax 'shellspec_subject_array'
function shellspec_subject_array() {
    shellspec_syntax_param count [ $# -ge 1 ] || return 0

    if ! [[ $(declare -p "$1" 2>/dev/null) =~ "declare -a" ]]; then
        unset SHELLSPEC_SUBJECT
    else
        local tmp="$1[@]"
        # shellcheck disable=SC2034  # variable appears unused
        SHELLSPEC_SUBJECT=("${!tmp+"${!tmp}"}")
    fi
    shift

    shellspec_syntax_dispatch modifier "$@"
}
