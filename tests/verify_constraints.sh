#!/bin/bash

set -Eeuo pipefail
shopt -s inherit_errexit

if [[ ! -v CI_PROJECT_DIR ]]; then
    export CI_PROJECT_DIR=${PWD}
    export CI_JOB_NAME=job
    export CI_JOB_ID=1
    export CI_PIPELINE_ID=1
    export CKI_CURL_CONFIG_FILE=${CKI_CURL_CONFIG_FILE:-/dev/null}
fi

CONSTRAINTS=$(python3 -m cki_lib.yaml dump --format json < pipeline/constraints.yml | jq -r .variables.CONSTRAINTS)
GITLAB_COM_PACKAGES=$(python3 -m cki_lib.yaml dump --format json < cki_pipeline.yml | jq -r '.".with_python_image".variables.GITLAB_COM_PACKAGES')
export GITLAB_COM_PACKAGES
export DATA_PROJECTS=
export SOFTWARE_DIR=${CI_PROJECT_DIR}/software
export SOFTWARE_VERSIONS_ARTIFACTS_PATH=constraints.txt.new
export VENV_DIR=${SOFTWARE_DIR}/venv
export VENV_PY3=${VENV_DIR}/bin/python3
export MAX_TRIES=5
export MAX_WAIT=5

# shellcheck source-path=SCRIPTDIR/..
source tests/source_functions.sh

setup_software

cki_echo_heading "📦 Comparing constraints.txt against YAML version"
txt_constraints=$(cat constraints.txt)
yaml_constraints=${CONSTRAINTS}
if ! diff -u <(echo "${txt_constraints}") <(echo "${yaml_constraints}"); then
    cki_echo_error "  pipeline/constraints.yml outdated, check the lines above for details"
    echo "  and update the file accordingly"
    exit 1
fi

cki_echo_heading "📦 Comparing package selection against constraints file"
previous_selection=$(sed '/^#/d;s/=.*//' constraints.txt | sort -u)
new_selection=$(sed '/git+https/d;s/=.*//' "${SOFTWARE_VERSIONS_ARTIFACTS_PATH}" | sort -u)
diff -u <(echo "${previous_selection}") <(echo "${new_selection}") || true
diff=$(diff <(echo "${previous_selection}") <(echo "${new_selection}") || true)
if grep -q '^>' <<< "${diff}"; then
    cki_echo_error "  packages added, check the lines above for details"
    echo "  and update constraints.txt accordingly"
    exit 1
fi
if grep -q '^<' <<< "${diff}"; then
    cki_echo_notify "  packages removed, check the lines above for details"
    echo "  and update constraints.txt accordingly"
    exit 17
fi
