---
# gitlab-yaml-shellcheck: main=../../cki_pipeline.yml

.build-prepare: |
  # 🛠 Prepare to compile the kernel.
  kcidb_build set-time start_time now
  kcidb_build set comment "CKI build of ${kcidb_tree_name:-${name}}"
  # Update the state file with architecture details and package name.
  set_kernel_architecture_debug
  if is_debug_build; then
    kcidb_build set misc/package_name "${package_name%-debug}-debug"
  else
    kcidb_build set misc/package_name "${package_name}"
  fi
  # Save the compiler version
  if [[ "${compiler}" == 'clang' ]] ; then
    # clang does not use cross compile prefixes
    kcidb_build set compiler "$(clang --version | head -1)"
  else  # default, no other values supported
    kcidb_build set compiler "$("${CROSS_COMPILE}"gcc --version | head -1)"
  fi
  dump_rpm_nvrs
  # Mark the build stage as failed for now and replace it with a successful
  # status if we make it all the way through the stage later.
  kcidb_build set log_url "$(artifact_url "${BUILDLOG_PATH}")"
  # Set the path to the ccache tarball stored in S3.
  export CCACHE_FILENAME="${kcidb_tree_name:-${name}}-${CI_JOB_NAME##* }.tar"
  # Create a directory for fixing up kernels
  mkdir -p "${KERNEL_CLEANUP_DIR}"

.build-prepare-rpm: |
  # 🛠  Prepare directories to hold binaries and set up macros
  if [[ "${make_target}" = 'rpm' ]]; then
    set_build_package_versions

    # Make a directory to hold the RPMs.
    mkdir -p "${WORKDIR}/rpms/${ARCH_CONFIG}/"
    # Set up RPM macros for this build.
    echo "%_rpmdir ${WORKDIR}/rpms" | tee -a ~/.rpmmacros

    # In RHEL9 /tmp is a tmpfs and it's removed in every reboot.
    # To use gcov we need to change the default rpmbuild path
    # ("/tmp/rpmbuild")
    # rpm-ostree has many directories as 'ro', but /opt should be fine
    # https://github.com/coreos/rpm-ostree/issues/233
    if is_true "${coverage}"; then
      echo "%_topdir /opt/ckibuild" | tee -a ~/.rpmmacros
    fi

    # Override dist tag if specified
    if [ -n "${disttag_override}" ] ; then
      cki_echo_notify "Manual disttag override requested: ${disttag_override}"
      echo "%dist ${disttag_override}" | tee -a ~/.rpmmacros
    fi
  fi

.build-prepare-tarball: |
  # 🗒 Extract kernel source and get configuration file for tarballs.
  if [[ ${make_target} = 'targz-pkg' ]]; then
    if [[ "${compiler}" == 'clang' ]] ; then
      if [[ "${ARCH_CONFIG}" == "s390x" ]]; then
        # lld is not supported with s390x: https://github.com/ClangBuiltLinux/linux/issues/1524
        MAKE=(make "CC=clang")
      else
        MAKE=(make "LLVM=1")
      fi
    else
      MAKE=(make)
    fi

    tar -C "${WORKDIR}/" -xzf "${KERNEL_TARGZ_PATH}"
    cd "${WORKDIR}"
      echo "Generating kernel configuration..."

      get_base_kernel_config "${kernel_config_url}"

      # We run 'make config_target' (defaults to olddefconfig) just in case new kernel
      # options exist that are missing from the downloaded config.
      "${MAKE[@]}" "${config_target}" 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"

      # Handle kernel oops as kernel panic, that should cause the machine to reboot
      # and next tests to run in more stable environment.
      scripts/config --enable PANIC_ON_OOPS 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"

      # Clear trusted keys which are not required for our tests and would fail on upstream builds
      scripts/config --set-val CONFIG_SYSTEM_TRUSTED_KEYS "" 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"

      if is_true "${build_selftests}" && [[ -z "${CROSS_COMPILE}" ]] && is_debug_build; then
        # Enable all config options required by selftests
        "${MAKE[@]}" kselftest-merge 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
        # increase CONFIG_LOCKDEP_BITS: https://datawarehouse.internal.cki-project.org/issue/473
        scripts/config --set-val CONFIG_LOCKDEP_BITS 16 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
        # increase MAX_LOCKDEP_CHAINS: https://datawarehouse.internal.cki-project.org/issue/468
        scripts/config --set-val CONFIG_LOCKDEP_CHAINS_BITS 17 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
        # NOTE the config_target variable differs from the trigger variable from now
        # on in the build stage!
        config_target="${config_target} + kselftest-merge"
      else
        # Disable CONFIG_DEBUG_INFO to conserve resources and time unless building
        # selftests, as BPF selftests require this config.
        scripts/config --disable DEBUG_INFO 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
      fi

      # Enable KASAN when building debug kernel
      if is_debug_build; then
        # uncompressed modules can create large initramfs
        scripts/config --disable MODULE_COMPRESS_NONE 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
        scripts/config --enable MODULE_COMPRESS_XZ 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
        scripts/config --enable KASAN 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
      fi

      # Enable realtime preemption if this is a realtime kernel tree
      if is_true "${rt_kernel}" ; then
        scripts/config --enable PREEMPT_RT_FULL 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
      fi
    cd "${CI_PROJECT_DIR}"

    # Save all possible information in advance in case of build failure
    CONFIG_FILE=artifacts/kernel-${kcidb_tree_name:-${name}}-${KCIDB_BUILD_ID//:/_}.config
    cp "${WORKDIR}/.config" "${CI_PROJECT_DIR}/${CONFIG_FILE}"
    kcidb_build set config_name "${config_target}"
    kcidb_build set config_url "$(artifact_url "${CONFIG_FILE}")"
    cki_echo_heading "Generated kernel configuration available as ${CONFIG_FILE} in the job artifacts."

    # Remove the source tarball as it is no longer needed.
    rm -fv "${KERNEL_TARGZ_PATH}"
  fi

.build-restore-ccache: |
  if is_true "${CCACHE_ENABLE}"; then
    # Restore the ccache objects from S3.
      echo "Restoring ccache..."
      aws_s3_download BUCKET_CCACHE "${CCACHE_FILENAME}" "${CCACHE_DIR}.tar" &
      if wait $!; then
        tar --directory "${CCACHE_DIR}" --no-same-owner --no-same-permissions --no-xattrs --touch -xf "${CCACHE_DIR}.tar"
      fi
      rm -f "${CCACHE_DIR}.tar"
      ccache -sz
  fi

.build-print-variables: |
  # Print build environment variables for local reproducer runs.
  cki_echo_notify "Exported variables:"
  cki_echo_notify "    ARCH=${ARCH}"
  if [ -n "${CROSS_COMPILE}" ] ; then
    cki_echo_notify "    CROSS_COMPILE=${CROSS_COMPILE}"
  fi

.build-compile-tarball: |
  # 📥 Compile the kernel into a tarball.
  if [[ ${make_target} = 'targz-pkg' ]]; then
    # Compile the kernel.
    cd "${WORKDIR}"
      export KERNEL_RELEASE
      KERNEL_RELEASE="$("${MAKE[@]}" -s kernelrelease 2>/dev/null | tail -n1)"
      kcidb_checkout set misc/kernel_version "${KERNEL_RELEASE}"

      if is_debug_build; then
        # don't remove debug symbols from modules,
        # this information might be necessary when decoding call traces
        MAKE_CMD="${MAKE[*]} -j${MAKE_JOBS} targz-pkg"
      else
        MAKE_CMD="${MAKE[*]} -j${MAKE_JOBS} INSTALL_MOD_STRIP=1 targz-pkg"
      fi
      kcidb_build set command "${MAKE_CMD}"
      cki_echo_notify "Compiling the kernel with: ${MAKE_CMD}"

      # Save timestamp before starting the build
      store_time
      # KBUILD_BUILD_TIMESTAMP should be set to a deterministic value between builds in order to avoid 100% cache misses
      # https://docs.kernel.org/kbuild/reproducible-builds.html#timestamps
      KBUILD_BUILD_TIMESTAMP='' ${MAKE_CMD} 2>&1 | ts -s >> "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
      # Calculate and save build duration after ending the build
      kcidb_build set-int duration "$(calculate_duration)"
      export TARBALL_FILE=artifacts/kernel-${kcidb_tree_name:-${name}}-${KCIDB_BUILD_ID//:/_}.tar.gz
      mv linux-*.tar.gz "${CI_PROJECT_DIR}/${TARBALL_FILE}"

      cki_echo_heading "Kernel compiled successfully, binary available as ${TARBALL_FILE} in the job artifacts."

      # Build selftests if configured. Only build for native builds with selftest configs:
      # - We don't have native libs needed as we use a single root and installing
      #   multiple same ones leads to overwrites; so we can't cross-build selftests
      # - selftest configs pull in debug options and we don't want to mess with the
      #   regular build and only want to build selftests for the branch that has the
      #   debug configs
      if is_true "${build_selftests}" && [[ -z "${CROSS_COMPILE}" ]] && is_debug_build; then
        cki_echo_notify "Building kernel selftests..."

        if [[ -z "${selftest_subsets}" ]] ; then
          # Override the value for easier building, it's not used for anything else
          selftest_subsets="all"
        fi

        successful_subsets=()

        for subset in ${selftest_subsets} ; do
          # Make sure we don't set the variable if we're actually trying to build everything
          if [[ "${subset}" != "all" ]] ; then
            gen_target="TARGETS=${subset}"
          fi

          # Save actual retcode from make command for each subset. We don't want the pipeline
          # to fail/abort when selftests builds fail, so we have to work around our -e and
          # pipefail settings via {} || true
          { "${MAKE[@]}" -C tools/testing/selftests V=1 SKIP_TARGETS="" ${gen_target:+"${gen_target}"} FORCE_TARGETS=1 2>&1 | ts -s \
                    >> "${ARTIFACTS_DIR}/selftests_${subset}.log" ;
            make_retcode=${PIPESTATUS[0]} ; } || true
          if [ "${make_retcode}" = "0" ] ; then
            successful_subsets+=("${subset}")
          else
            cki_echo_error "Building ${subset} selftests failed, check out $(artifact_url "artifacts/selftests_${subset}.log") for more details."
          fi
          kcidb_add_selftest "${subset}" "${make_retcode}"
        done

        # Empty arrays fail as "unbound variable" at Bash < 4.3 (RHEL 7)
        if [ "${#successful_subsets[@]}" -gt 0 ] && [[ "${selftest_subsets}" != "all" ]]; then
          gen_target=TARGETS="${successful_subsets[*]}"
        fi

        # Save the retcode, not just logs. Use {} || true to avoid aborting the pipeline.
        # Make the log name for our custom target match canonical_subtest_name from KCIDB
        { "${MAKE[@]}" -C tools/testing/selftests gen_tar V=1 SKIP_TARGETS="" ${gen_target:+"${gen_target}"} 2>&1 | ts -s \
                  >> "${ARTIFACTS_DIR}/selftests_install_and_packaging.log" ;
          make_retcode=${PIPESTATUS[0]} ; } || true
        kcidb_add_selftest "install and packaging" "${make_retcode}"
        mv "tools/testing/selftests/kselftest_install/kselftest-packages/${SELFTESTS_TARGZ_PATH##*/}" \
           "${CI_PROJECT_DIR}/${SELFTESTS_TARGZ_PATH}"

      fi
    cd "${CI_PROJECT_DIR}"

    cd "${KERNEL_CLEANUP_DIR}"
      tar -xf "${CI_PROJECT_DIR}/${TARBALL_FILE}"

      if [[ -d "linux-${KERNEL_RELEASE}-${ARCH}/boot" ]]; then
          mv "linux-${KERNEL_RELEASE}-${ARCH}/boot" boot
          mv "linux-${KERNEL_RELEASE}-${ARCH}/lib" lib
      fi
      lib_dir="lib/modules/${KERNEL_RELEASE}"
      source_dir="usr/src/kernels/${KERNEL_RELEASE}"
      # Fixup /source and /build symlinks. These are broken because tarball building expects running
      # on the same machine as the kernels are built which is not our case. RPM packages already handle
      # this situation.
      rm -f "${lib_dir}/build"
      rm -f "${lib_dir}/source"
      mkdir -p "${source_dir}"
      # Use excludes from mkspec. Also add "tar-install" directory to them as we don't need it.
      excludes=("*vmlinux*" "*.mod" "*.o" "*.ko" "*.cmd" "Documentation" ".config.old" ".missing-syscalls.d" "*.s" "tar-install")
      excludes=("${excludes[@]/#/--exclude=}")
      # Explicitly keep Documentation/Kconfig as it's needed for building modules,
      # and include/asm-generic/vmlinux.lds.h which is a >=v5.11 addition. We do want
      # to add the vmlinux.lds.h here instead of excluding all possible vmlinux binaries
      # in the excludes line as we could miss some which we don't want to happen.
      tar cf - -C "${WORKDIR}" Documentation/Kconfig include/asm-generic/vmlinux.lds.h "${excludes[@]}" . | tar xf - -C "${source_dir}"
      ln -sfT "/usr/src/kernels/${KERNEL_RELEASE}" "${lib_dir}/build"
      ln -sfT "/usr/src/kernels/${KERNEL_RELEASE}" "${lib_dir}/source"

      if [ -n "${CROSS_COMPILE}" ] ; then
          # Fixes for cross compilation - remove broken parts
          rm -f "${source_dir}/scripts/basic/fixdep"
          rm -f "${source_dir}/scripts/mod/mk_elfconfig"
          rm -f "${source_dir}/scripts/mod/modpost"
          rm -f "${source_dir}/tools/bpf/resolve_btfids/resolve_btfids"
          rm -f "${source_dir}/tools/bpf/resolve_btfids/fixdep"
          rm -f "${source_dir}/tools/bpf/resolve_btfids/libbpf.a"
          rm -f "${source_dir}/tools/bpf/resolve_btfids/libbpf/libbpf.a"
          rm -f "${source_dir}/tools/objtool/fixdep"
      fi

      # Move uncompressed binary away from /boot if possible to not take up space
      if [ -f "boot/vmlinuz-${KERNEL_RELEASE}" ] || [ -f "boot/vmlinux-kbuild-${KERNEL_RELEASE}" ] ; then
        mv "boot/vmlinux-${KERNEL_RELEASE}" "${lib_dir}/"
      fi

      # And finally, put the tarball back together!
      tar --owner=root --group=root -zcf "${CI_PROJECT_DIR}/${TARBALL_FILE}" boot lib usr
    cd "${CI_PROJECT_DIR}"
  fi

.build-compile-rpm: |
  # 📦 Compile the kernel into an RPM.
  # Only do this step if we are building RPMs.
  if [[ ${make_target} = 'rpm' ]]; then
    RPMBUILD_WITH_array=()
    RPMBUILD_WITHOUT_array=()
    create_array_from_string RPMBUILD_WITH
    create_array_from_string RPMBUILD_WITHOUT

    # Add nondefault modifiers from triggers
    rpmbuild_with_array=()
    create_array_from_string rpmbuild_with
    RPMBUILD_WITH_array+=("${rpmbuild_with_array[@]}")

    # Override the defaults for native compilation as we can build more things here.
    if [[ -z "${CROSS_COMPILE}" ]] ; then
      RPMBUILD_WITHOUT_array=(trace)
      remove_from_array RPMBUILD_WITH_array cross
    fi

    # Enable debug config if we're running the debug build but not on regular job!
    if is_true "${DEBUG_KERNEL}"; then
      RPMBUILD_WITH_array+=(debug)
      # zfcpdump kernel on the normal jobs is a hack and not supported for debug
      RPMBUILD_WITHOUT_array+=(up zfcpdump)
    fi

    if [[ "${compiler}" == 'clang' ]] ; then
      RPMBUILD_WITH_array+=(toolchain_clang)
    fi

    if is_true "${coverage}"; then
      RPMBUILD_WITH_array+=(gcov)
    fi

    # Exclude all kernel variants except the ones explicitly picked to build
    export RPMBUILD_VARIANTS_array=()
    create_array_from_string RPMBUILD_VARIANTS
    remove_kernel_variants "${RPMBUILD_WITH_array[@]}"
    RPMBUILD_WITHOUT_array+=("${RPMBUILD_VARIANTS_array[@]}")

    rpmbuild_args=(--target "${ARCH_CONFIG}")
    for elt in "${RPMBUILD_WITH_array[@]}"; do
        rpmbuild_args+=(--with "${elt}")
    done
    for elt in "${RPMBUILD_WITHOUT_array[@]}"; do
        rpmbuild_args+=(--without "${elt}")
    done
    # Write our make options to the kcidb file.
    kcidb_build set command "rpmbuild ${rpmbuild_args[*]}"
    cki_echo_notify "Compiling the kernel with: rpmbuild --rebuild ${rpmbuild_args[*]}"

    # Save timestamp before starting the build
    store_time
    # Build the arch-specific kernel RPMs.
    # KBUILD_BUILD_TIMESTAMP should be set to a deterministic value between builds in order to avoid 100% cache misses
    # https://docs.kernel.org/kbuild/reproducible-builds.html#timestamps
    KBUILD_BUILD_TIMESTAMP='' rpmbuild --rebuild "${rpmbuild_args[@]}" \
      artifacts/*.src.rpm 2>&1 | ts -s >> "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"

    # Calculate and save build duration after ending the build
    kcidb_build set-int duration "$(calculate_duration)"

    # Artifact all RPMs so we can create the repo later
    mv "${WORKDIR}/rpms/"*/*.rpm "${ARTIFACTS_DIR}/"

    artifact_config_from_rpm

    cki_echo_heading "Kernel compilation succeeded. Check the publish stage for full yum/dnf repository."
  fi

.build-save-ccache: |
  if is_true "${CCACHE_ENABLE}"; then
    # NOTE(mhayden): This hack is required because tar thinks that it is root
    # inside the container and it preserves ownership/modes/etc. When it tries
    # to restore with files that have the SUID bit set, it fails. The recursive
    # chmod here is gross, but it fixes the problem completely.
    echo "Storing ccache..."
    chmod -R 0777 "${CCACHE_DIR}"
    ccache -s
    tar --directory "${CCACHE_DIR}" --create --file "${CCACHE_DIR}.tar" .
    aws_s3_upload BUCKET_CCACHE "${CCACHE_DIR}.tar" "${CCACHE_FILENAME}"
    rm "${CCACHE_DIR}.tar"
  fi

.build-tag-tests-expected: |
  # Populate field in misc to let DataWarehouse know we will test this build.
  if ! is_true "${skip_test}"; then
    kcidb_build set-bool misc/test_plan_missing true
  fi

.build_native_x86_64-noarch:
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder x86_64
      merge
  needs:
    - {artifacts: false, job: prepare builder x86_64}
    - {artifacts: false, job: merge}
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-build-noarch-runner

.build_native_x86_64:
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder x86_64
      merge
      build noarch
  needs:
    - {artifacts: false, job: prepare builder x86_64}
    - {artifacts: false, job: merge}
    - {artifacts: false, job: build noarch, optional: true}
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-build-runner-x86_64

.build_native_ppc64le:
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder ppc64le
      merge
      build noarch
    CROSS_COMPILE: ""
  needs:
    - {artifacts: false, job: prepare builder ppc64le}
    - {artifacts: false, job: merge}
    - {artifacts: false, job: build noarch, optional: true}
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-build-runner-ppc64le

.build_native_aarch64:
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder aarch64
      merge
      build noarch
    CROSS_COMPILE: ""
  needs:
    - {artifacts: false, job: prepare builder aarch64}
    - {artifacts: false, job: merge}
    - {artifacts: false, job: build noarch, optional: true}
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-build-runner-aarch64

.build_native_s390x:
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder s390x
      merge
      build noarch
    CROSS_COMPILE: ""
  needs:
    - {artifacts: false, job: prepare builder s390x}
    - {artifacts: false, job: merge}
    - {artifacts: false, job: build noarch, optional: true}
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-build-runner-s390x

.build_native_riscv64:
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder riscv64
      merge
      build noarch
    CROSS_COMPILE: ""
  needs:
    - {artifacts: false, job: prepare builder riscv64}
    - {artifacts: false, job: merge}
    - {artifacts: false, job: build noarch, optional: true}
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-build-runner-riscv64

.build:
  extends: [.with_artifacts, .with_retries, .with_timeout, .with_builder_image]
  stage: build
  variables:
    ARTIFACTS: >
      artifacts/*.rpm
      artifacts/*.config
      artifacts/*.tar.*
      artifacts/*.log
      ${SELFTESTS_TARGZ_PATH}
      ${MR_DIFF_PATH}
      ${FILE_LIST_PATH}
      ${SOURCES_FILE_LIST_PATH}
      ${SUBSYSTEM_SOURCES_FILE_LIST_PATH}
      ${BUILDROOT_CONTENTS_PATH_PREFIX}-*.txt
  before_script:
    - !reference [.common-before-script]
  script:
    - |
      kcidb_build append-dict misc/provenance function="executor" url="${CI_JOB_URL}" service_name="gitlab"
    - !reference [.build-prepare]
    - !reference [.build-prepare-tarball]
    - !reference [.build-prepare-rpm]
    - !reference [.build-restore-ccache]
    - !reference [.build-print-variables]
    - !reference [.build-compile-tarball]
    - !reference [.build-compile-rpm]
    - !reference [.build-save-ccache]
    - !reference [.build-tag-tests-expected]
  after_script:
    - !reference [.common-after-script-head]
    - |
      if [[ "${CI_JOB_STATUS}" == "failed" ]]; then
        kcidb_build set-bool valid false
        print_and_save_build_failure "${KCIDB_BUILD_ID}"
      fi
    - !reference [.common-after-script-tail]
  rules:
    - !reference [.skip_without_stage]
    - !reference [.skip_without_source]
    - !reference [.skip_without_debug]
    - !reference [.skip_without_architecture]
    - when: on_success

.build-compile-noarch: |
  # 📦 Compile noarch artifacts into an RPM.
  if [[ ${make_target} = 'rpm' ]]; then
    cki_echo_notify "Building noarch packages"
    kcidb_build set command 'rpmbuild --rebuild --target noarch'

    # Save timestamp before starting the build
    store_time
    # Build the arch-independent kernel RPMs.
    rpmbuild --rebuild --target noarch artifacts/*.src.rpm 2>&1 | ts -s >> "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"

    # Calculate and save build duration after ending the build
    kcidb_build set-int duration "$(calculate_duration)"

    # Artifact all RPMs so we can create the repo later
    mv "${WORKDIR}/rpms/"*/*.rpm "${ARTIFACTS_DIR}/"

    cki_echo_heading "Building of noarch packages succeeded. Check the publish stage for full yum/dnf repository."
  fi

.build-noarch:
  extends: [.with_artifacts, .with_retries, .with_timeout, .with_builder_image]
  stage: build
  variables:
    ARTIFACTS: >
      artifacts/*.noarch.rpm
      artifacts/*.log
  before_script:
    - !reference [.common-before-script]
  script:
    - |
      kcidb_build append-dict misc/provenance function="executor" url="${CI_JOB_URL}" service_name="gitlab"
    - !reference [.build-prepare]
    - !reference [.build-prepare-rpm]
    - !reference [.build-compile-noarch]
    - kcidb_build set-bool valid true
  after_script:
    - !reference [.common-after-script-head]
    - |
      if [[ "${CI_JOB_STATUS}" == "failed" ]]; then
        kcidb_build set-bool valid false
        print_and_save_build_failure "${KCIDB_BUILD_ID}"
      fi
    - !reference [.common-after-script-tail]
  rules:
    - !reference [.skip_without_stage]
    - !reference [.skip_without_source]
    - !reference [.skip_without_architecture]
    - !reference [.skip_without_rpm]
    - !reference [.skip_without_noarch]
    - when: on_success

# for >= RHEL8 supported arches ppc64le/aarch64/s390x, the .build_native_* ->
# .build_runner_* mapping happens in pipeline/dependencies/build-*.yml

build noarch:
  extends: [.build-noarch, .noarch_variables, .build_native_x86_64-noarch]

build i686:
  extends: [.build, .i686_variables, .build_native_x86_64]

build x86_64:
  extends: [.build, .x86_64_variables, .build_native_x86_64]

build x86_64 debug:
  extends: [.build, .x86_64_variables, .debug_variables, .build_native_x86_64]

build ppc64le:
  extends: [.build, .ppc64le_variables, .build_runner_ppc64le]

build ppc64le debug:
  extends: [.build, .ppc64le_variables, .debug_variables, .build_runner_ppc64le]

build aarch64:
  extends: [.build, .aarch64_variables, .build_runner_aarch64]

build aarch64 debug:
  extends: [.build, .aarch64_variables, .debug_variables, .build_runner_aarch64]

build ppc64:
  extends: [.build, .ppc64_variables, .build_native_x86_64]

build s390x:
  extends: [.build, .s390x_variables, .build_runner_s390x]

build s390x debug:
  extends: [.build, .s390x_variables, .debug_variables, .build_runner_s390x]

build riscv64:
  extends: [.build, .riscv64_variables, .build_runner_riscv64]

build riscv64 debug:
  extends: [.build, .riscv64_variables, .debug_variables, .build_runner_riscv64]
